const http = require("http");
const port = process.env.PORT || 3000;
const host = process.env.HOST || "127.0.0.1";

const server = http.createServer((req, res) => {
    class Router {
        constructor() {
            this.endpoints = {}
        }
        request(method='get', path, handler) {

        }
    }
    switch (req.method) {
        case "GET": {
            switch (req.url) {
                case "/home": {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "text/plain");
                    res.end("Home page\n");
                    break;
                }
            }
            break;
        }
        case "POST": {
            switch (req.url) {
                case "/api/admin": {
                    res.statusCode = 200;
                    res.setHeader("Content-Type", "text/plain");
                    res.end("Create admin request\n");
                    break;
                }
            }
            break;
        }
    }
});

server.listen(port, host, () => {
    console.log(`Server listens http://${host}:${port}`);
});

const Emitter = require('events');
const emitter = new Emitter();

emitter.on('message', (data) => {
    console.log('You send message' + data);
})

const MESAGE = process.env.message || '';

if (MESAGE) {
    emitter.emit('message', MESAGE, 123);
} else {
    emitter.emit('message', ' You dont set message');
}
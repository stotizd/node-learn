const path = require('path');
const fs = require('fs');

fs.mkdir(path.resolve(__dirname, 'folder'), (err) => {
    if(err) {
        console.log(err);
    } else {
        console.log('Folder created');
    }
});

fs.rmdir(path.resolve(__dirname, 'folder'), (err) => {
    if(err) {
        console.log(err);
    } else {
        console.log('Folder deleted');
    }
});

const createdFileAsync = async(path, data) => {
    return new Promise((resolve, rejects) => { 
        fs.writeFile(path, data, (err) => {
            if(err) {
                return rejects(err)
            };
            console.log('File created');
            resolve();
        })
    });
}
const appendFileAsync = async(path, data) => {
    return new Promise((resolve, rejects) => {
        fs.appendFile(path, data, (err) => {
            if (err) {
                return rejects(err)
            };
            console.log('File appended');
            resolve();
        })
    })
}
const readFileAsync = async(path) => {
    return new Promise((resolve, rejects) => {
        fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
            if (err) {
                return rejects(err)
            }
            console.log('File readed');
            resolve(data);
        })
    })
}
const removeFileAsync = async(path) => {
    return new Promise((resolve, rejects) => {
        fs.rm(path, (err) => {
            if (err) {
                return rejects(err)
            }
            console.log('File deleted');
            resolve();
        })
    })
}

createdFileAsync(path.resolve(__dirname, 'text.txt'), 'init')
    .then(() => {appendFileAsync(path.resolve(__dirname, 'text.txt'), ' ulalala')})
    .then(() => {readFileAsync(path.resolve(__dirname, 'text.txt'))
        .then((data) => {console.log(data)})
        .then(() => {
            removeFileAsync(path.resolve(__dirname, 'text.txt'))
        })
    })
    .catch((err) => {console.log(err)})

const path = require('path');

const wrongPath = 'first/second/third'; //неверная запись, путь в зависимости от OC может отличаться написанием.

path.join('first', 'second', 'third'); //first/second/third
path.join(__dirname, 'first', 'second', 'third'); // /Users/azakoreo/Desktop/edu/node/path/first/second/third
path.join(__filename ,'first', 'second', 'third'); // /Users/azakoreo/Desktop/edu/node/path/path.js/first/second/third

/* ABSOLUTE PATH */
console.log(path.join(__dirname)); // /Users/azakoreo/Desktop/edu/node/path
console.log(path.resolve()); // /Users/azakoreo/Desktop/edu/node/